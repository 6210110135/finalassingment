import axios from "axios";
import { useRef } from "react";
import { useState } from "react";
import { useHistory } from "react-router-dom";
import "./register.scss";

export default function Register() {
  const [email, setEmail] = useState("");
  const [password, setPassword] = useState("");
  const [username, setUsername] = useState("");
  const history = useHistory();

  const emailRef = useRef();
  const passwordRef = useRef();
  const usernameRef = useRef();

  const handleStart = () => {
    setEmail(emailRef.current.value);
  };

  const handleFinish = async (e) => {
    e.preventDefault();
    setPassword(passwordRef.current.value);
    setUsername(usernameRef.current.value);
    try {
      await axios.post("auth/register", { email,username, password });
      history.push("/login");
    } catch (err) {}
  };
  return (
    <div className="register">
      <div className="top">
        <div className="wrapper">
          <img
            className="logo"
            src="https://th.bing.com/th/id/R.6c617dcfcc6933dae210f6bb0c1c71d2?rik=0npmGpf0lrWEfg&riu=http%3a%2f%2fpluspng.com%2fimg-png%2fmovie-png-hd-movie-logo-cliparts-2524910-1118.png&ehk=G3GfIkpmGGFizSQXbqYq%2fWy9Pf5St7ECppgIhUxpdEc%3d&risl=&pid=ImgRaw&r=0"
            alt=""
          />        
            <button className="loginButton">
              Sign In
            </button>    
        </div>
      </div>
      <div className="container">
      <h1>Welcome to Review movie</h1>
              <h2>Watch anywhere.</h2>
              <p>
                Ready to watch? Enter your email address to create or restart your web
              </p>
        {!email ? (
          <div className="input">
            <input type="email" placeholder="email address" ref={emailRef} />
            <button className="registerButton" onClick={handleStart}>
              Get Started
            </button>
          </div>
        ) : (
          <form className="input">
            <input type="username" placeholder="username" ref={usernameRef} />
            <input type="password" placeholder="password" ref={passwordRef} />
            <button className="registerButton" onClick={handleFinish}>
              Start
            </button>
          </form>
        )}
      </div>
    </div>
  );
}