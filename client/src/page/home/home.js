import Navbar from "../../componests/navbar/Navbar";
import Featured from "../../componests/featured/Featured";
import List from "../../componests/List/List";
import "./home.scss";
import { useState, useEffect } from "react";
import axios from "axios";

const Home = ({type}) => {
  const [lists, setLists] = useState([]);
  const [genre, setGenre] = useState(null);

  useEffect(() =>{ 
    const getRandomLists = async () =>{
      try{
        const res =await axios.get(`list${type ? "?type=" + type : ""}${genre ? "&genre=" + genre : ""}`,{
          headers: {
            token: 
              "Bearer eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJpZCI6IjYyM2M5YjdlY2ZiMzcxZDI4N2Q1OTU1NSIsImlzQWRtaW4iOmZhbHNlLCJpYXQiOjE2NDg4NjY5NDcsImV4cCI6MTY0OTI5ODk0N30.62CHNG816cdCcfqCrufccF6nnOMW4JF6-Y58jetCb7U "
          }
        }
        );
        setLists(res.data);
      }catch(err){
        console.log(err);
      }
    };
    getRandomLists();
  },[type, genre]);
  return (
    <div className="home">
      <Navbar />
      <Featured type={type} setGenre={setGenre} />
      {lists.map((list) => (
        <List list={list} />
      ))}
    </div>
  );
};
export default Home;