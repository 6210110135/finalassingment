import firebase from 'firebase/compat/app';
import 'firebase/compat/auth';

const firebaseConfig = firebase.initializeApp({
    apiKey: "AIzaSyAp3J1ELoe0WW4gnS26Jq-ivqSga4PzkyY",
    authDomain: "react-auth-90592.firebaseapp.com",
    projectId: "react-auth-90592",
    storageBucket: "react-auth-90592.appspot.com",
    messagingSenderId: "515115752746",
    appId: "1:515115752746:web:c166a8fb16adc9f1205253",
    measurementId: "G-Y54QZTKFQK"
  });

  export default firebaseConfig;