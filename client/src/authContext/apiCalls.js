import axios from "axios";

export const login = async (user, dispatch) => {
  dispatch({
    type: "LOGIN_START",
  });
  try {
    const res = await axios.post("auth/login", user);
    dispatch( {
      type: "LOGIN_SUCCESS",
      payload: user});
  } catch (err) {
    dispatch({type: "LOGIN_FAILURE",});
  }
};
