FROM node:14-alpine3.15
WORKDIR /app
COPY package.json /app
RUN npm ci --only=production && npm clean --force
COPY . /app
CMD node sever/index.js
EXPOSE 8800